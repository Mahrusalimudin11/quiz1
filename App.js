import {Text, Button, View, TouchableOpacity, TextInput} from 'react-native';
import React, {Component} from 'react';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      warnaText: 'red',
      text: '',
      text1: '',
      fullText: '',
    };
  }

  ubahwarna = () => {
    const warna = this.state.warnaText;
    if (warna === 'black') {
      this.setState({warnaText: 'blue'});
    } else if (warna === 'blue') {
      this.setState({warnaText: 'green'});
    } else {
      this.setState({warnaText: 'red'});
    }
  };

  fullname = () => {
    alert('full name : ' + this.state.text + ' ' + this.state.text1);
  };
  render() {
    return (
      <View>
        <Text style={{color: this.state.warnaText}}>HOME</Text>
        <TouchableOpacity onPress={() => this.ubahwarna()}>
          <Text>Klik</Text>
        </TouchableOpacity>
        <TextInput
          style={{
            height: 45,
            borderColor: 'black',
            borderWidth: 1,
            fontSize: 18,
          }}
          onChangeText={text => this.setState({text: text})}
          placeholder="First Name"
          value={this.state.text}
        />
        <TextInput
          style={{
            height: 45,
            borderColor: 'black',
            borderWidth: 1,
            fontSize: 18,
          }}
          onChangeText={text1 => this.setState({text1: text1})}
          placeholder="Last Name"
          value={this.state.text1}
        />
        <Button title="fullname" color="blue" onPress={this.fullname} />
      </View>
    );
  }
}

export default App;
